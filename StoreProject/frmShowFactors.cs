﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreProject
{
    public partial class frmShowFactors : Form
    {
        public frmShowFactors()
        {
            InitializeComponent();
        }

        private void frmShowFactors_Load(object sender, EventArgs e)
        {
            DataBase db = new DataBase();
            dataGridView1.DataSource = db.ShowData("select factornum, userid, firstname, lastname, productid, pname, tb_factor.count, tb_factor.price from tb_factor, tb_user, tb_product WHERE (productid = tb_product.id) AND (userid = tb_user.id)");
            CBSearchBy.SelectedIndex = 0;
            int totalPrice = 0;
            for (int i = 0; i < dataGridView1.RowCount - 1; i++)
            {
                totalPrice += Convert.ToInt32(dataGridView1.Rows[i].Cells[7].Value);
            }
            lblTotalSold.Text = totalPrice.ToString();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            int factornum = Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value);
            int userid = Convert.ToInt32(dataGridView1.CurrentRow.Cells[1].Value);
            int pid = Convert.ToInt32(dataGridView1.CurrentRow.Cells[4].Value);
            //MessageBox.Show(factornum.ToString() + "..." + userid.ToString() + "..." + pid.ToString());
            DataBase db = new DataBase();
            if (db.Executecmd("delete from tb_factor where (factornum=" + factornum + ") AND (productid=" + pid + ") AND (userid=" + userid + ")") == true)
            {
                MessageBox.Show("این قسمت از فاکتور با موفقیت حذف گردید.");
                dataGridView1.DataSource = db.ShowData("select factornum, userid, firstname, lastname, productid, pname, tb_factor.count from tb_factor, tb_user, tb_product WHERE(productid = tb_product.id) AND(userid = tb_user.id)");
            }
            else
            {
                MessageBox.Show("خطا، مجددا امتحان کنید!");
            }
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (txtSearchExp.Text == "")
            {
                DataBase x = new DataBase();
                dataGridView1.DataSource = x.ShowData("select factornum, userid, firstname, lastname, productid, pname, tb_factor.count, tb_factor.price from tb_factor, tb_user, tb_product WHERE (productid = tb_product.id) AND (userid = tb_user.id)");
            }
            else if (CBSearchBy.SelectedIndex == 0)
            {
                DataBase db = new DataBase();
                dataGridView1.DataSource = db.ShowData("select factornum, userid, firstname, lastname, productid, pname, tb_factor.count, tb_factor.price from tb_factor, tb_user, tb_product WHERE (productid = tb_product.id) AND (userid = tb_user.id) AND (factornum = " + txtSearchExp.Text + ")");
            }
            else if (CBSearchBy.SelectedIndex == 1)
            {
                DataBase db = new DataBase();
                dataGridView1.DataSource = db.ShowData("select factornum, userid, firstname, lastname, productid, pname, tb_factor.count, tb_factor.price from tb_factor, tb_user, tb_product WHERE (productid = tb_product.id) AND (userid = tb_user.id) AND (userid = " + txtSearchExp.Text + ")");
            }
            else if (CBSearchBy.SelectedIndex == 2)
            {
                DataBase db = new DataBase();
                dataGridView1.DataSource = db.ShowData("select factornum, userid, firstname, lastname, productid, pname, tb_factor.count, tb_factor.price from tb_factor, tb_user, tb_product WHERE (productid = tb_product.id) AND (userid = tb_user.id) AND (firstname like\"%" + txtSearchExp.Text + "%\")");
            }
            else if (CBSearchBy.SelectedIndex == 3)
            {
                DataBase db = new DataBase();
                dataGridView1.DataSource = db.ShowData("select factornum, userid, firstname, lastname, productid, pname, tb_factor.count, tb_factor.price from tb_factor, tb_user, tb_product WHERE (productid = tb_product.id) AND (userid = tb_user.id) AND (lastname like\"%" + txtSearchExp.Text + "%\")");
            }
            else if (CBSearchBy.SelectedIndex == 4)
            {
                DataBase db = new DataBase();
                dataGridView1.DataSource = db.ShowData("select factornum, userid, firstname, lastname, productid, pname, tb_factor.count, tb_factor.price from tb_factor, tb_user, tb_product WHERE (productid = tb_product.id) AND (userid = tb_user.id) AND (productid = " + txtSearchExp.Text + ")");
            }
            else if (CBSearchBy.SelectedIndex == 5)
            {
                DataBase db = new DataBase();
                dataGridView1.DataSource = db.ShowData("select factornum, userid, firstname, lastname, productid, pname, tb_factor.count, tb_factor.price from tb_factor, tb_user, tb_product WHERE (productid = tb_product.id) AND (userid = tb_user.id) AND (pname like\"%" + txtSearchExp.Text + "%\")");
            }
        }
    }
}
