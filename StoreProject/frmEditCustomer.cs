﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreProject
{
    public partial class frmEditCustomer : Form
    {
        public frmEditCustomer(Customers customers)
        {
            InitializeComponent();
            lblid.Text = customers.id.ToString();
            txtfirstname.Text = customers.firstName.ToString();
            txtlastname.Text = customers.lastName.ToString();
            txtphone.Text = customers.phone.ToString();
            txtaddress.Text = customers.useraddr.ToString();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            DataBase db = new DataBase();
            string query = "UPDATE tb_user SET [firstname]='" + txtfirstname.Text.ToString() + "', [lastname]='" + txtlastname.Text.ToString() + "', [useraddr]='" + txtaddress.Text.ToString() + "', [phone]=" + txtphone.Text.ToString() + " WHERE [id]=" + Convert.ToInt32(lblid.Text) + "";
            if (db.Executecmd(query) == true)
            {
                MessageBox.Show("کاربر مورد نظر با موفقیت ویرایش گردید.");
            }
            else
            {
                MessageBox.Show("خطا! مجددا امتحان کنید.");
            }
            this.Close();
        }
    }
}
