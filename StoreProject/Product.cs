﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoreProject
{
    public class Product
    {
        public int id { get; set; }
        public string pname { get; set; }
        public string category { get; set; }
        public int count { get; set; }
        public int price { get; set; }
    }
}
