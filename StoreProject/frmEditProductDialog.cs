﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreProject
{
    public partial class frmEditProductDialog : Form
    {
        public frmEditProductDialog(Product product)
        {
            InitializeComponent();
            lblid.Text = product.id.ToString();
            txtpname.Text = product.pname.ToString();
            txtcount.Text = product.count.ToString();
            txtprice.Text = product.price.ToString();
            for (int i=0; i<CBCat.Items.Count; i++)
            {
                if (product.category.ToString() == CBCat.Items[i].ToString())
                {
                    CBCat.SelectedItem = CBCat.Items[i];
                    break;
                }
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            DataBase db = new DataBase();
            string query = "UPDATE tb_product SET [pname]='" + txtpname.Text.ToString() + "', [category]='" + CBCat.SelectedItem.ToString() + "', [count]=" + Convert.ToInt32(txtcount.Text) + ", [price]=" + Convert.ToInt32(txtprice.Text) + " WHERE [id]=" + Convert.ToInt32(lblid.Text) + "";
            if (db.Executecmd(query) == true)
            {
                MessageBox.Show("محصول مورد نظر با موفقیت ویرایش گردید.");
            }
            else
            {
                MessageBox.Show("خطا! مجددا امتحان کنید.");
            }
            this.Close();
        }
    }
}
