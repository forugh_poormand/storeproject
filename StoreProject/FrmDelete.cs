﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreProject
{
    public partial class FrmDelete : Form
    {
        public FrmDelete()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("آیا مایل به حذف اطلاعات هستید؟", "هشدار", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                DataBase db = new DataBase();
                if (db.Executecmd("delete from Tbl_kharid where code_kharid=" + txtcode1.Text + "") == true)
                {
                    DataBase db1 = new DataBase();
                    dataGridView1.DataSource = db1.ShowData("select * from Tbl_kharid");
                    MessageBox.Show("حذف فاکتور با موفقیت انجام شد");
                }
                else
                {
                    MessageBox.Show("حذف فاکتور انجام نشد ");
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DataBase db = new DataBase();
            dataGridView1.DataSource = db.ShowData("select * from Tbl_kharid where code_kharid="+ txtcode1.Text + "");
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            if (MessageBox.Show("آیا مایل به حذف اطلاعات هستید؟", "هشدار", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                DataBase db = new DataBase();
                if (db.Executecmd("delete from Tbl_kala where kala_code1=" + txtcode1.Text + "") == true)
                {
                    DataBase db1 = new DataBase();
                    dataGridView1.DataSource = db1.ShowData("select * from Tbl_kala");
                    MessageBox.Show("حذف فاکتور با موفقیت انجام شد");
                }
                else
                {
                    MessageBox.Show("حذف فاکتور انجام نشد ");
                }
            }
        }
    }
}
