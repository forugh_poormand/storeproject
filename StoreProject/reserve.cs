﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreProject
{
    public partial class reserve : Form
    {
        public reserve()
        {
            InitializeComponent();
        }
        public string codeNum { get; set; }
        public string productName { get; set; }
        public int buyCount { get; set; }
        public int productPrice { get; set; }
        public int productTotalPrice { get; set; }
        public int factorCode { get; set; }
        public int prodcount { get; set; }
        public int currentID { get; set; }
        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void reserve_Load(object sender, EventArgs e)
        {
            factorCode = 1;
            Random rand = new Random();
            for (int i=0; i<4; i++)
            {
                factorCode *= 10;
                factorCode += rand.Next(0, 10);
            }
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            string userCode = txtuserid.Text;
            lblCustomerCode.Text = userCode;
            panelCustomerCode.Visible = false;
            panelCard.Visible = true;
            panelProducts.Visible = true;
            DataBase db = new DataBase();
            dataGridView2.DataSource = db.ShowData("select*from tb_product");
            CBCat.SelectedIndex = 0;
            CBSearchBy.SelectedIndex = 0;
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (CBCat.SelectedIndex == 0)
            {
                if (txtSearchExp.Text == "")
                {
                    DataBase x = new DataBase();
                    dataGridView2.DataSource = x.ShowData("select*from tb_product");
                }
                else if (CBSearchBy.SelectedIndex == 0)
                {
                    DataBase db = new DataBase();
                    dataGridView2.DataSource = db.ShowData("select * from tb_product where id=" + txtSearchExp.Text + "");
                }
                else if (CBSearchBy.SelectedIndex == 1)
                {
                    DataBase db = new DataBase();
                    dataGridView2.DataSource = db.ShowData("select * from tb_product where pname like\"%" + txtSearchExp.Text + "%\"");
                }
            }
            else
            {
                if (txtSearchExp.Text == "")
                {
                    DataBase x = new DataBase();
                    dataGridView2.DataSource = x.ShowData("select*from tb_product where category='" + CBCat.SelectedItem.ToString() + "'");
                }
                else if (CBSearchBy.SelectedIndex == 0)
                {
                    DataBase db = new DataBase();
                    dataGridView2.DataSource = db.ShowData("select * from tb_product where id=" + txtSearchExp.Text + " and  category='" + CBCat.SelectedItem.ToString() + "'");
                }
                else if (CBSearchBy.SelectedIndex == 1)
                {
                    DataBase db = new DataBase();
                    dataGridView2.DataSource = db.ShowData("select * from tb_product where pname like\"%" + txtSearchExp.Text + "%\" and  category='" + CBCat.SelectedItem.ToString() + "'");
                }
            }
        }
        public void changeIndex(object sender, EventArgs e)
        {
            DataBase db = new DataBase();
            if (CBCat.SelectedIndex == 0)
            {
                dataGridView2.DataSource = db.ShowData("select*from tb_product");
            }
            else
            {
                dataGridView2.DataSource = db.ShowData("select*from tb_product where category='" + CBCat.SelectedItem.ToString() + "'");
            }
        }

        private void btnAddtoCard_Click(object sender, EventArgs e)
        {
            codeNum = dataGridView2.CurrentRow.Cells[0].Value.ToString();
            productPrice = Convert.ToInt32(dataGridView2.CurrentRow.Cells[4].Value);
            prodcount = Convert.ToInt32(dataGridView2.CurrentRow.Cells[3].Value);
            panelCount.Visible = true;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            var totalPrice = 0;
            bool same = false;
            if (txtcount.Text != "")
            {
                buyCount = Convert.ToInt32(txtcount.Text);
                if (buyCount <= prodcount && buyCount >= 0)
                {
                    panelCount.Visible = false;
                    productTotalPrice = productPrice * buyCount;
                    if (dataGridView3.RowCount > 1)
                    {
                        for (int i = 0; i < dataGridView3.RowCount - 1; i++)
                        {
                            if (dataGridView3.Rows[i].Cells[1].Value.ToString() == codeNum.ToString())
                            {
                                dataGridView3.Rows[i].Cells[3].Value = Convert.ToInt32(dataGridView3.Rows[i].Cells[3].Value) + buyCount;
                                dataGridView3.Rows[i].Cells[5].Value = Convert.ToInt32(dataGridView3.Rows[i].Cells[3].Value) * productPrice;
                                DataBase db = new DataBase();
                                db.Executecmd("UPDATE tb_product SET [count]=" + (prodcount - buyCount) + " where [id]=" + codeNum + "");
                                dataGridView2.DataSource = db.ShowData("select*from tb_product");
                                for (int j = 0; j < dataGridView3.RowCount - 1; j++)
                                {
                                    totalPrice += Convert.ToInt32(dataGridView3.Rows[j].Cells[5].Value);
                                }
                                lblTotalPrice.Text = totalPrice.ToString();
                                same = true;
                                break;
                            }
                        }

                        if (!same)
                        {
                            this.dataGridView3.Rows.Add(factorCode, codeNum, lblCustomerCode.Text, buyCount, productPrice, productTotalPrice);
                            DataBase db = new DataBase();
                            db.Executecmd("UPDATE tb_product SET [count]=" + (prodcount - buyCount) + " where [id]=" + codeNum + "");
                            dataGridView2.DataSource = db.ShowData("select*from tb_product");
                            for (int i = 0; i < dataGridView3.RowCount - 1; i++)
                            {
                                totalPrice += Convert.ToInt32(dataGridView3.Rows[i].Cells[5].Value);
                            }
                            lblTotalPrice.Text = totalPrice.ToString();
                        }

                    }
                    else
                    {
                        this.dataGridView3.Rows.Add(factorCode, codeNum, lblCustomerCode.Text, buyCount, productPrice, productTotalPrice);
                        DataBase db = new DataBase();
                        db.Executecmd("UPDATE tb_product SET [count]=" + (prodcount - buyCount) + " where [id]=" + codeNum + "");
                        dataGridView2.DataSource = db.ShowData("select*from tb_product");
                        for (int i=0; i<dataGridView3.RowCount -1; i++)
                        {
                            totalPrice += Convert.ToInt32(dataGridView3.Rows[i].Cells[5].Value);
                        }
                        lblTotalPrice.Text = totalPrice.ToString();
                    }
                }
                else if (buyCount >= 0)
                {
                    MessageBox.Show("از این محصول فقط " + prodcount + " عدد موجود است!");
                }
                else
                {
                    MessageBox.Show("مقدار صحیحی را وارد کنید!");
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            int prodID;
            int backCount;
            DataBase db = new DataBase();
            for (int i=0; i<dataGridView3.RowCount -1; i++)
            {
                prodID = Convert.ToInt32(dataGridView3.Rows[i].Cells[1].Value);
                backCount = Convert.ToInt32(dataGridView3.Rows[i].Cells[3].Value);
                db.Executecmd("UPDATE tb_product SET [count]=[count] + " + backCount + " where [id]=" + prodID + "");
                dataGridView3.Rows.RemoveAt(i);
            }
            this.Close();
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            int totalPrice = 0;
            int prodID;
            int backCount;
            DataBase db = new DataBase();
            prodID = Convert.ToInt32(dataGridView3.CurrentRow.Cells[1].Value);
            backCount = Convert.ToInt32(dataGridView3.CurrentRow.Cells[3].Value);
            db.Executecmd("UPDATE tb_product SET [count]=[count] + " + backCount + " where [id]=" + prodID + "");
            dataGridView3.Rows.RemoveAt(dataGridView3.CurrentRow.Index);
            dataGridView2.DataSource = db.ShowData("select * from tb_product");
            for (int i = 0; i < dataGridView3.RowCount - 1; i++)
            {
                totalPrice += Convert.ToInt32(dataGridView3.Rows[i].Cells[5].Value);
            }
            lblTotalPrice.Text = totalPrice.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            int factornum = factorCode;
            DataBase d = new DataBase();
            for (int i=0; i<dataGridView3.RowCount -1; i++)
            {
                int prodIDfac = Convert.ToInt32(dataGridView3.Rows[i].Cells[1].Value);
                int countfac = Convert.ToInt32(dataGridView3.Rows[i].Cells[3].Value);
                int pricefac = Convert.ToInt32(dataGridView3.Rows[i].Cells[5].Value);
                factorCode++;
                if (d.Executecmd("insert into tb_factor values('" + factorCode.ToString() + "','" + prodIDfac.ToString() + "','" + lblCustomerCode.Text + "','" + countfac.ToString() + "','" + pricefac.ToString() + "','" + factornum.ToString() + "')") == false)
                {
                    MessageBox.Show("خرید انجام نشد!");
                    return;
                }
            }
            MessageBox.Show("خرید با موفقیت انجام شد.");
            this.Close();
        }
    }
}
