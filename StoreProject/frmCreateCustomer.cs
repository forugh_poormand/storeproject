﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreProject
{
    public partial class frmCreateCustomer : Form
    {
        public frmCreateCustomer()
        {
            InitializeComponent();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            if (txtFirstName.Text == "" || txtLastName.Text == "" || txtCustomerCode.Text == "" || txtPhoneNumber.Text == "")
            {
                MessageBox.Show("لطفا فیلدهارا پرکنید");
            }
            else
            {
                DataBase d = new DataBase();

                if (d.Executecmd("insert into tb_user values('" + txtCustomerCode.Text + "','" + txtFirstName.Text + "','" + txtLastName.Text + "','" + txtAddress.Text + "','" + txtPhoneNumber.Text + "')") == true)
                {

                    MessageBox.Show(".ثبت مشتری با موفقیت انجام شد");
                }
                else
                {
                    MessageBox.Show("ثبت مشتری انجام نشد.");
                }

                DataBase x = new DataBase();
                dataGridView1.DataSource = x.ShowData("select*from tb_user");
            }
        }

        private void frmCreateCustomer_Load(object sender, EventArgs e)
        {
            DataBase x = new DataBase();
            dataGridView1.DataSource = x.ShowData("select*from tb_user");
        }
    }
}
