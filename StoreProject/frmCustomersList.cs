﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreProject
{
    public partial class frmCustomersList : Form
    {
        public frmCustomersList()
        {
            InitializeComponent();
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            if (txtSearchExp.Text == "")
            {
                DataBase x = new DataBase();
                dataGridView1.DataSource = x.ShowData("select*from tb_user");
            }
            else if (CBSearchBy.SelectedIndex == 0)
            {
                DataBase db = new DataBase();
                dataGridView1.DataSource = db.ShowData("select * from tb_user where id=" + txtSearchExp.Text + "");
            }
            else if (CBSearchBy.SelectedIndex == 1)
            {
                DataBase db = new DataBase();
                dataGridView1.DataSource = db.ShowData("select * from tb_user where firstname like\"%" + txtSearchExp.Text + "%\"");
            }
            else if (CBSearchBy.SelectedIndex == 2)
            {
                DataBase db = new DataBase();
                dataGridView1.DataSource = db.ShowData("select * from tb_user where lastname like\"%" + txtSearchExp.Text + "%\"");
            }
        }

        private void frmCustomersList_Load(object sender, EventArgs e)
        {
            DataBase x = new DataBase();
            dataGridView1.DataSource = x.ShowData("select*from tb_user");
            CBSearchBy.SelectedIndex = 0;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            int pid = Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value);
            DataBase db = new DataBase();
            if (db.Executecmd("delete from tb_user where id=" + pid + "") == true)
            {
                MessageBox.Show("مشتری با کد اشتراک " + pid + " با موفقیت حذف گردید.");
                dataGridView1.DataSource = db.ShowData("select*from tb_user");
            }
            else
            {
                MessageBox.Show("خطا، مجددا امتحان کنید!");
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            Customers customer = new Customers();
            customer.id = Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value);
            customer.firstName = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            customer.lastName = dataGridView1.CurrentRow.Cells[2].Value.ToString();
            customer.phone = dataGridView1.CurrentRow.Cells[4].Value.ToString();
            customer.useraddr = dataGridView1.CurrentRow.Cells[3].Value.ToString();
            frmEditCustomer frmEditCustomer = new frmEditCustomer(customer);
            frmEditCustomer.ShowDialog();
            DataBase db = new DataBase();
            dataGridView1.DataSource = db.ShowData("select*from tb_user");
        }
    }
}
