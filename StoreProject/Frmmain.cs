﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreProject
{
    public partial class Frmmain : Form
    {
        public Frmmain()
        {
            InitializeComponent();
        }

        private void Frmmain_Load(object sender, EventArgs e)
        {

        }

        private void خریدجدیدToolStripMenuItem_Click(object sender, EventArgs e)
        {
            reserve r = new reserve();
            r.ShowDialog();
        }

        private void حذفخریدToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmDelete f = new FrmDelete();
            
            f.ShowDialog();
        }

        private void کالایجدیدToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form2 f = new Form2();
            f.ShowDialog();
        }

        private void لیستکالاهاToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
        }

        private void جستجویکالاToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form3 f = new Form3();
            f.ShowDialog();
        }

        private void TSMICreateCustomer_Click(object sender, EventArgs e)
        {
            frmCreateCustomer frmCreateCustomer = new frmCreateCustomer();
            frmCreateCustomer.ShowDialog();
        }

        private void TSMICustomersList_Click(object sender, EventArgs e)
        {
            frmCustomersList frmCustomersList = new frmCustomersList();
            frmCustomersList.ShowDialog();
        }

        private void TSMIFactors_Click(object sender, EventArgs e)
        {
            frmShowFactors frmShowFactors = new frmShowFactors();
            frmShowFactors.ShowDialog();
        }
    }
}
