﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoreProject
{
    class DataBase
    {
        OleDbConnection con = new OleDbConnection("Provider=Microsoft.ACE.OLEDB.12.0;Data Source=F:\\University\\IT Basics\\ITProject\\storeproject\\StoreProject\\Database2.accdb");
        private OleDbDataAdapter da;
        private DataTable dt;
        private OleDbDataReader dr;

        public bool Executecmd(string comand)
        {
            try
            {
                OleDbCommand cmd = new OleDbCommand(comand, con);
                con.Open();
                cmd.ExecuteNonQuery();
                con.Close();
                return true;
            }
                catch
                {
                    return false;
                }
        }
        public DataTable ShowData(string comand)
        {
            da = new OleDbDataAdapter(comand, con);
            dt = new DataTable();
            con.Open();
            da.Fill(dt);
            con.Close();
            return dt;
        }
    }
}
