﻿namespace StoreProject
{
    partial class Frmmain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Frmmain));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.خریذToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.خریدجدیدToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.حذفخریدToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.کالاهاToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.کالایجدیدToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.جستجویکالاToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.کاربرانToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMICreateCustomer = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMICustomersList = new System.Windows.Forms.ToolStripMenuItem();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.آمارفروشToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.TSMIFactors = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.Color.SlateBlue;
            this.menuStrip1.Font = new System.Drawing.Font("B Kamran", 14F, System.Drawing.FontStyle.Bold);
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.خریذToolStripMenuItem,
            this.کالاهاToolStripMenuItem,
            this.کاربرانToolStripMenuItem,
            this.آمارفروشToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Padding = new System.Windows.Forms.Padding(6, 3, 0, 3);
            this.menuStrip1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.menuStrip1.Size = new System.Drawing.Size(668, 36);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // خریذToolStripMenuItem
            // 
            this.خریذToolStripMenuItem.BackColor = System.Drawing.Color.SlateBlue;
            this.خریذToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.خریدجدیدToolStripMenuItem,
            this.حذفخریدToolStripMenuItem});
            this.خریذToolStripMenuItem.Font = new System.Drawing.Font("B Kamran", 14F, System.Drawing.FontStyle.Bold);
            this.خریذToolStripMenuItem.Name = "خریذToolStripMenuItem";
            this.خریذToolStripMenuItem.Size = new System.Drawing.Size(45, 30);
            this.خریذToolStripMenuItem.Text = "خرید";
            // 
            // خریدجدیدToolStripMenuItem
            // 
            this.خریدجدیدToolStripMenuItem.BackColor = System.Drawing.Color.SlateBlue;
            this.خریدجدیدToolStripMenuItem.Name = "خریدجدیدToolStripMenuItem";
            this.خریدجدیدToolStripMenuItem.Size = new System.Drawing.Size(134, 30);
            this.خریدجدیدToolStripMenuItem.Text = "خرید جدید";
            this.خریدجدیدToolStripMenuItem.Click += new System.EventHandler(this.خریدجدیدToolStripMenuItem_Click);
            // 
            // حذفخریدToolStripMenuItem
            // 
            this.حذفخریدToolStripMenuItem.BackColor = System.Drawing.Color.SlateBlue;
            this.حذفخریدToolStripMenuItem.Name = "حذفخریدToolStripMenuItem";
            this.حذفخریدToolStripMenuItem.Size = new System.Drawing.Size(134, 30);
            this.حذفخریدToolStripMenuItem.Text = "حذف خرید";
            this.حذفخریدToolStripMenuItem.Click += new System.EventHandler(this.حذفخریدToolStripMenuItem_Click);
            // 
            // کالاهاToolStripMenuItem
            // 
            this.کالاهاToolStripMenuItem.BackColor = System.Drawing.Color.SlateBlue;
            this.کالاهاToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.کالایجدیدToolStripMenuItem,
            this.جستجویکالاToolStripMenuItem});
            this.کالاهاToolStripMenuItem.Font = new System.Drawing.Font("B Kamran", 14F, System.Drawing.FontStyle.Bold);
            this.کالاهاToolStripMenuItem.ForeColor = System.Drawing.Color.Black;
            this.کالاهاToolStripMenuItem.Name = "کالاهاToolStripMenuItem";
            this.کالاهاToolStripMenuItem.Size = new System.Drawing.Size(48, 30);
            this.کالاهاToolStripMenuItem.Text = "کالاها";
            // 
            // کالایجدیدToolStripMenuItem
            // 
            this.کالایجدیدToolStripMenuItem.BackColor = System.Drawing.Color.SlateBlue;
            this.کالایجدیدToolStripMenuItem.Name = "کالایجدیدToolStripMenuItem";
            this.کالایجدیدToolStripMenuItem.Size = new System.Drawing.Size(140, 30);
            this.کالایجدیدToolStripMenuItem.Text = "کالای جدید";
            this.کالایجدیدToolStripMenuItem.Click += new System.EventHandler(this.کالایجدیدToolStripMenuItem_Click);
            // 
            // جستجویکالاToolStripMenuItem
            // 
            this.جستجویکالاToolStripMenuItem.BackColor = System.Drawing.Color.SlateBlue;
            this.جستجویکالاToolStripMenuItem.Name = "جستجویکالاToolStripMenuItem";
            this.جستجویکالاToolStripMenuItem.Size = new System.Drawing.Size(140, 30);
            this.جستجویکالاToolStripMenuItem.Text = "لیست انبار";
            this.جستجویکالاToolStripMenuItem.Click += new System.EventHandler(this.جستجویکالاToolStripMenuItem_Click);
            // 
            // کاربرانToolStripMenuItem
            // 
            this.کاربرانToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSMICreateCustomer,
            this.TSMICustomersList});
            this.کاربرانToolStripMenuItem.Name = "کاربرانToolStripMenuItem";
            this.کاربرانToolStripMenuItem.Size = new System.Drawing.Size(58, 30);
            this.کاربرانToolStripMenuItem.Text = "مشتری";
            // 
            // TSMICreateCustomer
            // 
            this.TSMICreateCustomer.BackColor = System.Drawing.Color.SlateBlue;
            this.TSMICreateCustomer.Name = "TSMICreateCustomer";
            this.TSMICreateCustomer.Size = new System.Drawing.Size(180, 30);
            this.TSMICreateCustomer.Text = "ثبت مشتری جدید";
            this.TSMICreateCustomer.Click += new System.EventHandler(this.TSMICreateCustomer_Click);
            // 
            // TSMICustomersList
            // 
            this.TSMICustomersList.BackColor = System.Drawing.Color.SlateBlue;
            this.TSMICustomersList.Name = "TSMICustomersList";
            this.TSMICustomersList.Size = new System.Drawing.Size(180, 30);
            this.TSMICustomersList.Text = "لیست مشتریان";
            this.TSMICustomersList.Click += new System.EventHandler(this.TSMICustomersList_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("pictureBox1.BackgroundImage")));
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Location = new System.Drawing.Point(0, 39);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(231, 268);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("B Kamran", 25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label1.ForeColor = System.Drawing.Color.OrangeRed;
            this.label1.Location = new System.Drawing.Point(286, 104);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(330, 46);
            this.label1.TabIndex = 2;
            this.label1.Text = "به فروشگاه مواد غذایی خوش آمدید!";
            // 
            // label2
            // 
            this.label2.AllowDrop = true;
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.SlateBlue;
            this.label2.Font = new System.Drawing.Font("B Kamran", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label2.Location = new System.Drawing.Point(252, 176);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(385, 32);
            this.label2.TabIndex = 3;
            this.label2.Text = "با عرضه تمامی محصولات غذایی به صورت آنلاین و 24ساعته";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.SlateBlue;
            this.label3.Font = new System.Drawing.Font("B Kamran", 17F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.label3.Location = new System.Drawing.Point(376, 208);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(143, 32);
            this.label3.TabIndex = 4;
            this.label3.Text = "در خدمت شما هستیم";
            // 
            // آمارفروشToolStripMenuItem
            // 
            this.آمارفروشToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.TSMIFactors});
            this.آمارفروشToolStripMenuItem.Name = "آمارفروشToolStripMenuItem";
            this.آمارفروشToolStripMenuItem.Size = new System.Drawing.Size(78, 30);
            this.آمارفروشToolStripMenuItem.Text = "آمار فروش";
            // 
            // TSMIFactors
            // 
            this.TSMIFactors.BackColor = System.Drawing.Color.SlateBlue;
            this.TSMIFactors.Name = "TSMIFactors";
            this.TSMIFactors.Size = new System.Drawing.Size(180, 30);
            this.TSMIFactors.Text = "مشاهده فاکتور ها";
            this.TSMIFactors.Click += new System.EventHandler(this.TSMIFactors_Click);
            // 
            // Frmmain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSlateBlue;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.ClientSize = new System.Drawing.Size(668, 306);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.menuStrip1);
            this.DoubleBuffered = true;
            this.Font = new System.Drawing.Font("B Kamran", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Frmmain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Frmmain";
            this.Load += new System.EventHandler(this.Frmmain_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem خریذToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem خریدجدیدToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem کالاهاToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem کالایجدیدToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem حذفخریدToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem جستجویکالاToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ToolStripMenuItem کاربرانToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem TSMICreateCustomer;
        private System.Windows.Forms.ToolStripMenuItem TSMICustomersList;
        private System.Windows.Forms.ToolStripMenuItem آمارفروشToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem TSMIFactors;
    }
}