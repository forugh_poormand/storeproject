﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoreProject
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (CBCat.SelectedIndex == 0)
            {
                if (textBox1.Text == "")
                {
                    DataBase x = new DataBase();
                    dataGridView1.DataSource = x.ShowData("select*from tb_product");
                }
                else if (CBSearchBy.SelectedIndex == 0)
                {
                    DataBase db = new DataBase();
                    dataGridView1.DataSource = db.ShowData("select * from tb_product where id=" + textBox1.Text + "");
                }
                else if (CBSearchBy.SelectedIndex == 1)
                {
                    DataBase db = new DataBase();
                    dataGridView1.DataSource = db.ShowData("select * from tb_product where pname like\"%" + textBox1.Text + "%\"");
                }
            }
            else
            {
                if (textBox1.Text == "")
                {
                    DataBase x = new DataBase();
                    dataGridView1.DataSource = x.ShowData("select*from tb_product where category='" + CBCat.SelectedItem.ToString() + "'");
                }
                else if (CBSearchBy.SelectedIndex == 0)
                {
                    DataBase db = new DataBase();
                    dataGridView1.DataSource = db.ShowData("select * from tb_product where id=" + textBox1.Text + " and  category='" + CBCat.SelectedItem.ToString() + "'");
                }
                else if (CBSearchBy.SelectedIndex == 1)
                {
                    DataBase db = new DataBase();
                    dataGridView1.DataSource = db.ShowData("select * from tb_product where pname like\"%" + textBox1.Text + "%\" and  category='" + CBCat.SelectedItem.ToString() + "'");
                }
            }
        }

        private void Form3_Load(object sender, EventArgs e)
        {
            DataBase x = new DataBase();
            dataGridView1.DataSource = x.ShowData("select*from tb_product");
            CBCat.SelectedIndex = 0;
            CBSearchBy.SelectedIndex = 0;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            int pid = Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value);
            DataBase db = new DataBase();
            if (db.Executecmd("delete from tb_product where id=" + pid + "") == true)
            {
                MessageBox.Show("کالای " + pid + " با موفقیت حذف گردید.");
                dataGridView1.DataSource = db.ShowData("select*from tb_product");
            }
            else
            {
                MessageBox.Show("خطا، مجددا امتحان کنید!");
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            Product product = new Product();
            product.id = Convert.ToInt32(dataGridView1.CurrentRow.Cells[0].Value);
            product.pname = dataGridView1.CurrentRow.Cells[1].Value.ToString();
            product.category = dataGridView1.CurrentRow.Cells[2].Value.ToString();
            product.count = Convert.ToInt32(dataGridView1.CurrentRow.Cells[3].Value);
            product.price = Convert.ToInt32(dataGridView1.CurrentRow.Cells[4].Value);
            frmEditProductDialog frmEditProductDialog = new frmEditProductDialog(product);
            frmEditProductDialog.ShowDialog();
            DataBase db = new DataBase();
            dataGridView1.DataSource = db.ShowData("select*from tb_product");
        }

        public void changeIndex (object sender, EventArgs e)
        {
            DataBase db = new DataBase();
            if (CBCat.SelectedIndex == 0)
            {
                dataGridView1.DataSource = db.ShowData("select*from tb_product");
            }
            else
            {
                dataGridView1.DataSource = db.ShowData("select*from tb_product where category='" + CBCat.SelectedItem.ToString() + "'");
            }
        }
    }
}
